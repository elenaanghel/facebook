import java.util.Set;

public class User {
    private String name;
    private final Set<User> friends;
    private final Set<User> addedFriends;
    private final Set<User> friendRequest;
    private final Wall wall;

    public User(String name, Set<User> friends, Set<User> addedFriends, Set<User> friendRequest, Wall wall) {
        this.name = name;
        this.friends = friends;
        this.addedFriends = addedFriends;
        this.friendRequest = friendRequest;
        this.wall = wall;
    }

    Wall getWall(){ //vrem sa avem wallul
        return wall;
    }
    Post getLastPost(){
        return null;
    }
    public String showWall(User user){
        return null;
    }
    public String showWall(){
        return null;
    }

    public boolean addFriend(User user){
        return false;
    }
    public boolean receiveFriendRequest(User user){
        return false;
    }
    public boolean acceptFriend(User user){
        return false;
    }


}
