
import java.util.LinkedList;
import java.util.List;

public class Picture extends Post {

    private final List<User> tags = new LinkedList<>();
    private String description;

    public Picture(String description) {
        this.description = description;
    }

    public void addTag(User user) {
        tags.add(user);
    }

    public boolean removeTag(User user) {
        return tags.remove(user);
    }

    @Override
    public String show() {
        //TODO: update sshow properly

        return description + tags.toString();
    }
}
