public class Text extends Post {
    private String message;

    public Text(String message) {
        this.message = message;
    }

    public void setMessage(String message) {//daca avem show nu ne mai trebuie get
        this.message = message;
    }

    @Override
    public String show() {
       return message;
    }
}
