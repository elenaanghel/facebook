public abstract class Post {
    private Access access = Access.PUBLIC;//default e initializat cu null
    private int likes; //default e initializat cu 0

    public void setAccess(Access access) {
        this.access = access;
    }

    public Access getAccess() {
        return access;
    }

    public int getLikes() {
        return likes;
    }

    public void addLike() {
        likes++;
    }

    public abstract String show();
}
