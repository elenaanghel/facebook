import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;

public class Wall {

    private List<Post> posts;

    public Wall(Collection<Post> posts) {
        this.posts = new LinkedList<>(posts);
    }

    public boolean addPost(Post post) {
        return posts.add(post);
    }

    public boolean removePost(Post post) {
        return posts.remove(post);
    }

    public boolean editPost(Post oldPost, Post newPost) { //stim deja postul si vrem sa il schimbam cu acesta curent
        int oldPostIndex = posts.indexOf(oldPost);
        if(oldPostIndex<0){
            return false;
        }
        posts.set(oldPostIndex, newPost);
        return true;
    }
}
